﻿using System;
using System.Collections.Generic;

namespace Unitofwork.Modelss;

public partial class Product
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public int? CategoryId { get; set; }
}
