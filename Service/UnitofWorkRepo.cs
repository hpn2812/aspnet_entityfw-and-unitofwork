﻿using Unitofwork.Controllers;
using Unitofwork.Interface;
using Unitofwork.Modelss;

namespace Unitofwork.Repos
{
    public class UnitofWorkRepo : IUnitofWork
    {
        public ICategoryRepository categoryRepository { get; private set; }

        public IProductRepository productRepository { get; private set; }


        private readonly LearnDbContext learnDbContext;

        public UnitofWorkRepo(LearnDbContext learnDbContext)
        {
            this.learnDbContext = learnDbContext;
            categoryRepository = new CategoryRepository(learnDbContext);
            productRepository  = new ProductRepository(learnDbContext);
        }

        public async Task CompleteAsync()
        {
            await this.learnDbContext.SaveChangesAsync();
        }
    }
}
