﻿using Microsoft.EntityFrameworkCore;
using Unitofwork.Interface;
using Unitofwork.Modelss;

namespace Unitofwork.Repos
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(LearnDbContext learnDb) : base(learnDb)
        {
        }

        public override async Task<bool> AddEntity(Category entity)
        {
            try
            {
                await DbSet.AddAsync(entity);
                return true;

            }
            catch (Exception )
            {
                throw;
            }
        }

        public override async Task<bool> DeleteEntity(int id)
        {
            var existdata = await DbSet.FirstOrDefaultAsync(item => item.Id == id);
            if (existdata != null)
            {
                DbSet.Remove(existdata);
                return true;
            }
            else
            {
                return false;
            }
        }

        public override Task<List<Category>> GetAllAsync()
        {
            //get all category but not has products
            // return base.GetAllAsync();


            //get all category has products 
            return DbSet.Include(c => c.Products).ToListAsync();
        }

        public override async Task<Category> GetAsync(int id)
        {
            return await DbSet.FirstOrDefaultAsync(item => item.Id == id);
        }

        public override async Task<bool> UpdateEntity(Category entity)
        {
            try
            {
                var existdata = await DbSet.FirstOrDefaultAsync(item => item.Id == entity.Id);
                if (existdata != null)
                {
                    existdata.Id = entity.Id;
                    existdata.Name = entity.Name;
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
