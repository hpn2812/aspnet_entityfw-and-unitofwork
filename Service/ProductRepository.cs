﻿using Microsoft.EntityFrameworkCore;
using Unitofwork.Interface;
using Unitofwork.Modelss;

namespace Unitofwork.Repos
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(LearnDbContext learnDb) : base(learnDb)
        {
        }

        public override async Task<bool> AddEntity(Product entity)
        {
            try
            {
                await DbSet.AddAsync(entity);
                return true;

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public override async Task<bool> DeleteEntity(int id)
        {
            var existdata = await DbSet.FirstOrDefaultAsync(item => item.Id == id);
            if (existdata != null)
            {
                DbSet.Remove(existdata);
                return true;
            }
            else
            {
                return false;
            }
        }

        public override Task<List<Product>> GetAllAsync()
        {
            return base.GetAllAsync();
        }

        public override async Task<Product> GetAsync(int id)
        {
            return await DbSet.FirstOrDefaultAsync(item => item.Id == id);
        }

        public override async Task<bool> UpdateEntity(Product entity)
        {
            try
            {
                var existdata = await DbSet.FirstOrDefaultAsync(item => item.Id == entity.Id);
                if (existdata != null)
                {
                    existdata.Id = entity.Id;
                    existdata.Name = entity.Name;
                    existdata.CategoryId = entity.CategoryId;

                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
