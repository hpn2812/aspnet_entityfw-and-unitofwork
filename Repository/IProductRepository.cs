﻿using Unitofwork.Modelss;

namespace Unitofwork.Interface
{
    public interface IProductRepository : IGenericRepository<Product>
    {
    }
}
