﻿using Unitofwork.Modelss;

namespace Unitofwork.Interface
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
    }
}
