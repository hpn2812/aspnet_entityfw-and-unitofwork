﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Unitofwork.Modelss;

namespace Unitofwork.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IUnitofWork unitofWork;
        public ProductController(IUnitofWork work) {
            this.unitofWork = work;
        }
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var _data= await unitofWork.productRepository.GetAllAsync();
            return Ok(_data);
        }

        [HttpGet("Getbycode/{id}")]
        public async Task<IActionResult> Getbycode(int id)
        {
            var _data = await unitofWork.productRepository.GetAsync(id);
            return Ok(_data);
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create(Product product)
        {
            var _data = await unitofWork.productRepository.AddEntity(product);
            await unitofWork.CompleteAsync();
            return Ok(_data);
        }
        [HttpPut("Update")]
        public async Task<IActionResult> Update(Product product)
        {
            var _data = await unitofWork.productRepository.UpdateEntity(product);
            await unitofWork.CompleteAsync();
            return Ok(_data);
        }
        [HttpDelete("Remove")]
        public async Task<IActionResult> Remove(int id)
        {
            var _data = await unitofWork.productRepository.DeleteEntity(id);
            await unitofWork.CompleteAsync();
            return Ok(_data);
        }
    }
}
