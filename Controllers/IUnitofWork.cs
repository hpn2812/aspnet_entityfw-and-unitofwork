﻿using Unitofwork.Interface;

namespace Unitofwork.Controllers
{
    public interface IUnitofWork
    {
        ICategoryRepository categoryRepository { get; }

        IProductRepository productRepository { get; }

        Task CompleteAsync();
    }
}
