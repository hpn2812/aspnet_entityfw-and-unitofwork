﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Unitofwork.Modelss;

namespace Unitofwork.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly IUnitofWork unitofWork;
        public CategoryController(IUnitofWork work) {
            this.unitofWork = work;
        }
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var _data= await unitofWork.categoryRepository.GetAllAsync();
            return Ok(_data);
        }

        [HttpGet("Getbycode/{id}")]
        public async Task<IActionResult> Getbycode(int id)
        {
            var _data = await unitofWork.categoryRepository.GetAsync(id);
            return Ok(_data);
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create(Category category)
        {
            var _data = await unitofWork.categoryRepository.AddEntity(category);
            await unitofWork.CompleteAsync();
            return Ok(_data);
        }
        [HttpPut("Update")]
        public async Task<IActionResult> Update(Category category)
        {
            var _data = await unitofWork.categoryRepository.UpdateEntity(category);
            await unitofWork.CompleteAsync();
            return Ok(_data);
        }
        [HttpDelete("Remove")]
        public async Task<IActionResult> Remove(int id)
        {
            var _data = await unitofWork.categoryRepository.DeleteEntity(id);
            await unitofWork.CompleteAsync();
            return Ok(_data);
        }
    }
}
